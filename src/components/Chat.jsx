import { v4 as uuidv4 } from 'uuid';
import React, { Component } from "react";
import { connect } from 'react-redux';
import { Preloader } from './Preloader';
import { Header } from './Header';
import { MessageList } from './MessageList';
import { MessageInput } from './MessageInput';
import { EditModal } from './EditModal';
import * as actions from '../messages/actions';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.onEdit = this.onEdit.bind(this);
        this.onEditCancel = this.onEditCancel.bind(this);
        this.onEditSave = this.onEditSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
		this.onAdd = this.onAdd.bind(this);
    } 

    onEdit(id) {
		this.props.showEditModal();
        this.props.setEditedMessageId(id);
        this.props.disablePage();
	}

    onEditCancel(){
        this.props.hideEditModal();
        this.props.clearEditedMessageId();
        this.props.clearEditedMessage();
        this.props.enablePage();
    }

    onEditSave(){
        if (this.props.editedMessage !== "") {
            this.props.updateMessage(this.props.editedMessage);
            this.props.hideEditModal();
            this.props.clearEditedMessageId();
            this.props.clearEditedMessage();
            this.props.enablePage();
        } 
    }

	onDelete(id) {
		this.props.deleteMessage(id);
	}

	onAdd(data) {
		this.props.addMessage(data);
	}

    LastMessageFormatter = new Intl.DateTimeFormat("ru", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    });

    componentDidMount() {
        fetch(this.props.url)
          .then(res => res.json())
          .then(data => {
            this.props.loadData(data);
            this.props.hidePreloader();
          });
    }

    countUsers(messages) {
        let res = 1;
        for (let i = 1; i < messages.length; i++) {
            let j = 0;
            for (j = 0; j < i; j++)
                if (messages[i].user === messages[j].user)
                    break;
            if (i === j)
                res++;
        }
        return res;
    }

    sendMessage() {
        if (this.props.newMessage !== "") {
            const timeNow = new Date();
            const newMessage = {
                id: uuidv4(),
                userId: this.props.ownUserId,
                avatar: "https://t2.genius.com/unsafe/440x440/https%3A%2F%2Fimages.genius.com%2F09acba93727a483f8ed0579ba60dce61.715x715x1.jpg",
                user: "Templar",
                text: this.props.newMessage,
                createdAt: timeNow,
                editedAt: ""
            }
            this.onAdd(newMessage);
            this.props.clearNewMessage();
        }
    }

    onInputChange(event) {
        this.props.setNewMessage(event.target.value);
    }

    onInputChangeEdit(event) {
        this.props.setEditedMessage(event.target.value);
    }

    getEditModalContent() {
        const messages = this.props.messages;
        for (let i = 0; i < messages.length; i++) {
            if (messages[i].id === this.props.editedMessageId) {
                return <EditModal 
                            messageText={messages[i].text}
                            messageId={messages[i].id}
                            onInputChange={(event) => this.onInputChangeEdit(event)}
                            onEditCancel={() => this.onEditCancel()}
                            onEditSave={() => this.onEditSave()}
                            isShown={true}
                        />;
            }
        }
    }

    onKeyDown(event) {
        if (event.key === "ArrowUp" && !this.props.isPageDisabled) {
            const messages = this.props.messages;
            if(messages[messages.length - 1].userId === this.props.ownUserId) {
                const messageId = messages[messages.length - 1].id;
                this.onEdit(messageId);
            }
        }
    }

    arrowFunction() {
        document.addEventListener("keydown", (event) => this.onKeyDown(event));
    }

    render() {
        const messages = this.props.messages;
        this.arrowFunction();
        return (
            <>
                {this.props.preloader ? 
                    <Preloader /> 
                :
                    <> 
                        <Header 
                            chatTitle="My chat" 
                            userCount={messages ? (messages.length > 0 ? this.countUsers(messages) : 0) : 0}
                            messageCount={messages ? messages.length : 0}
                            lastMessageDate={messages ? (messages.length > 0 ? this.LastMessageFormatter.format(new Date(messages[messages.length - 1].createdAt)).replace(/,/g, '') : "none") : "none"}
                        >
                        </Header>
                        <MessageList
                            data={messages}
                            onDataChange={this.onDelete}
                            onMessageEdit={this.onEdit}
                            ownId={this.props.ownUserId}
                            disablePage={this.props.isPageDisabled}
                        >
                        </MessageList>
                        <MessageInput
                            value={this.props.newMessage}
                            onInputChange={(event) => this.onInputChange(event)}
                            sendMessage={(event) => this.sendMessage(event)}
                            type={"Send"}
                            disablePage={this.props.isPageDisabled}
                        >
                        </MessageInput>
                        {this.props.editModal ? this.getEditModalContent() : null}
                    </>
                }
            </>
        );
    }
}

const mapStateToProps = (state) => {
	return {
		messages: state.chat.messages,
        preloader: state.chat.preloader,
        editModal: state.chat.editModal,
        newMessage: state.chat.newMessage,
        editedMessage: state.chat.editedMessage,
        editedMessageId: state.chat.editedMessageId,
        ownUserId: state.chat.ownUserId,
        isPageDisabled: state.chat.disablePage
	}
};

const mapDispatchToProps = {
	...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);