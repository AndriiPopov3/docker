import './styles/ownMessage.css';

export function OwnMessage(props) {
    const MessageFormatter = new Intl.DateTimeFormat("ru", {
        hour: "2-digit",
        minute: "2-digit"
    });

    return (
        <div className="own-message">
            <div className="message-container">
                <div className="message-text">{props.text}</div>
                <div className="message-info-container">
                    <div className="message-time">{MessageFormatter.format(new Date(props.messageTime))}</div>
                    <button
                        className="message-edit"
                        onClick={() => props.onEdit(props.id)}
                        disabled={props.disabled ? true : false}
                    >
                        Edit
                    </button>
                    <button
                        className="message-delete"
                        onClick={() => props.onDelete(props.id)}
                        disabled={props.disabled ? true : false}
                    >
                        Delete
                    </button>
                </div>
            </div>
        </div>
    );
}