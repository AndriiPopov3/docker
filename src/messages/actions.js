import { 
    ADD_MESSAGE, 
    UPDATE_MESSAGE, 
    DELETE_MESSAGE, 
    LOAD_DATA, 
    HIDE_PRELOADER, 
    SHOW_EDIT_MODAL, 
    HIDE_EDIT_MODAL, 
    SET_EDITED_MESSAGE, 
    CLEAR_EDITED_MESSAGE,
    SET_EDITED_MESSAGE_ID, 
    CLEAR_EDITED_MESSAGE_ID,
    SET_NEW_MESSAGE,
    CLEAR_NEW_MESSAGE,
    DISABLE_PAGE,
    ENABLE_PAGE
 } from "./actionTypes";

export const loadData = data => ({
    type: LOAD_DATA,
    payload: {
        data
    }
});

export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        data
    }
});

export const updateMessage = data => ({
    type: UPDATE_MESSAGE,
    payload: {
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const setEditedMessageId = id => ({
    type: SET_EDITED_MESSAGE_ID,
    payload: {
        id
    }
});

export const clearEditedMessageId = () => ({
    type: CLEAR_EDITED_MESSAGE_ID
});

export const setEditedMessage = text => ({
    type: SET_EDITED_MESSAGE,
    payload: {
        text
    }
});

export const clearEditedMessage = () => ({
    type: CLEAR_EDITED_MESSAGE
});

export const setNewMessage = message => ({
    type: SET_NEW_MESSAGE,
    payload: {
        message
    }
});

export const clearNewMessage = () => ({
    type: CLEAR_NEW_MESSAGE
});

export const hidePreloader = () => ({
    type: HIDE_PRELOADER
});

export const showEditModal = () => ({
    type: SHOW_EDIT_MODAL
});

export const hideEditModal = () => ({
    type: HIDE_EDIT_MODAL
});

export const disablePage = () => ({
    type: DISABLE_PAGE
});

export const enablePage = () => ({
    type: ENABLE_PAGE
});