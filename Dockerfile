FROM node:12.18-alpine

COPY src ./src
COPY public ./public
COPY package*.json ./
RUN npm install
CMD ["npm", "start"]
EXPOSE 3000